package sudoku.model;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.Random;


public class Filler {
	
	
	public Filler(){
		
	}
	
	int [] [] sudoku9 = new int[] [] {
	      { 0, 8, 0, 4, 0, 2, 0, 6, 0 },
	      { 0, 3, 4, 0, 0, 0, 9, 1, 0 },
	      { 9, 6, 0, 0, 0, 0, 0, 8, 4 },
	      { 0, 0, 0, 2, 1, 6, 0, 0, 0 },
	      { 2, 0, 0, 0, 0, 9, 6, 0, 0 },
	      { 0, 1, 0, 3, 5, 7, 0, 0, 8 },
	      { 8, 4, 0, 0, 0, 0, 0, 7, 5 },
	      { 0, 2, 6, 0, 0, 0, 1, 3, 0 },
	      { 0, 9, 0, 7, 0, 1, 0, 4, 0 }
	    } ;
	    
	    int [] [] sudoku4 = new int[] [] {
	        { 2, 0, 0, 3 },
	        { 0, 3, 2, 0 },
	        { 0, 4, 0, 0 },
	        { 0, 0, 1, 0 }
	      } ;
	
	
	private int getRandom4(){
		Random rand = new Random();
		int x = rand.nextInt(3);
		return x;
	}
	
	private int getRadom9(){
		int randomNum = ThreadLocalRandom.current().nextInt(0, 8);
		return randomNum;
	}
	
	public void fillBoard(List<Square> squares, int size){
		//int sqrtOfSize = (int)Math.sqrt(size);
		//randomizeBoard();
		List<Square> filledSquares = null;
		
		if(size == 4){
			int[][] sudokuShuffled = randomizeBoard(4,sudoku4);
			for (int x = 0; x < size; x++) { // store in column-major
	            for (int y = 0; y < size; y++) {
	            	for (Square sq: squares) {
	                    if (sq.getX() == x && sq.getY() == y) {
	                         sq.setValue(sudokuShuffled[x][y]);
	                    }
	                }
	            }
	        }
		}
		
		if(size == 9){
			int[][] sudokuShuffled1 = randomizeBoard(9,sudoku9);
			for (int x = 0; x < size; x++) { // store in column-major
	            for (int y = 0; y < size; y++) {
	            	for (Square sq: squares) {
	                    if (sq.getX() == x && sq.getY() == y) {
	                         sq.setValue(sudokuShuffled1[x][y]);
	                    }
	                }
	            }
	        }
		}
		
	}
	
	public int[][] randomizeBoard(int size, int[][] board){
		int [][] newBoard = new int[size][size];
		int[] random = new int [20];
		
		for(int i = 0; i< random.length; i++){
			int randomNumb = getRandom4();
			random[i] = randomNumb;
		}
		
		for(int i = 0; i< random.length; i++){
			if(random[i]==0){
				newBoard = rotateRight(board);
			}
		}
		
		for(int i = 0; i< random.length; i++){
			if(random[i]==1){
				newBoard = mirrorRight(newBoard);
			}
		}
		
		for(int i = 0; i< random.length; i++){
			if(random[i]==2){
				newBoard = mirrorDown(newBoard);
			}
		}
		
		
		return newBoard;
		
		
	}
	
	public int[][] rotateRight(int [][] board){
		int [][] newBoard = new int[board.length][board.length];
		for(int i = 0; i<board.length; i++){
			for(int j = 0; j< board.length; j++){
				newBoard[i][j]= board[board.length-j-1][i];
			}
		}
		return newBoard;
		
	}
	
	public int[][] mirrorRight(int [][] board){
		int [][] newBoard = new int[board.length][board.length];
		
		for(int i = 0; i<board.length; i++){
			for(int j = 0; j< board.length; j++){
					newBoard[i][board.length - j -1] = board[i][j]; 
			}
		}
		
		
		
		return newBoard;
		
	}
	
	public int[][] mirrorDown(int [][] board){
		int [][] newBoard = new int[board.length][board.length];
		
		for(int i = 0; i<board.length; i++){
			for(int j = 0; j< board.length; j++){
				newBoard[board.length - i -1 ][j] = board[i][j];
			}
		}
		
		
		return newBoard;
		
	}

}
