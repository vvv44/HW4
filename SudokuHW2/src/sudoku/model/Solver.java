package sudoku.model;


public class Solver {
	/**
	 * A copy of the main board
	 */
	Board board;
	/**
	 * The board that is trying to be solved
	 */
	Board newB;
	/**
	 * Constructor
	 * @param board
	 */
	public Solver(Board board){
		this.board = board;
	}
	
	/**Clones the board into newB to avoid reference crossing*/
	public void cloneBoard(){
		newB = new Board(board.getSize());
		for(int i=0;i<newB.getSize();i++)
			for(int j=0;j<newB.getSize();j++){
				newB.setPosition(i, j);
				newB.enterValue(board.getSquare(i, j).getValue());
			}
	}	
	
	/**
	 * Checks if the insertion of @param num is valid in the coordenates of @cell
	 * in the board
	 */
	private boolean isValid(Square cell, int num) {
		int currentRow = cell.getX();
		int currentCol = cell.getY();
		for(int c = 0 ; c < newB.getSize() ; c++) {
			if(newB.getSquare(currentRow, c).getValue() == num)
				return false;
		}
		for(int r = 0 ; r < newB.getSize() ; r++) {
			if(newB.getSquare(r, currentCol).getValue() == num)
				return false;
		}
		int root = (int)Math.sqrt(newB.getSize());
		int x1 = root * (currentRow / root);
		int y1 = root * (currentCol / root);
		int x2 = x1 + root-1;
		int y2 = y1 + root-1;
		for(int x = x1 ; x <= x2 ; x++) {
			for (int y = y1 ; y <= y2 ; y++) {
				if(newB.getSquare(x, y).getValue() == num)
					return false;
			}
		}
		return true;
	}
	
	/**
	 * will return a sqare object with the coordinates that are next in line, 
	 * null if we it reaches the end.
	 * @param cell
	 * @return
	 */
	private Square getNext(Square cell) {
		int currentRow = cell.getX();
		int currentCol = cell.getY();
		currentCol++;
		int size = newB.getSize();
		if(currentCol > size - 1) {
			currentCol = 0;
			currentRow++;
		}
		if(currentRow > size - 1)
			return null;
		return new Square(currentRow, currentCol, 0);
	}
	/**
	 * this is going to try to solve the copy of the main board
	 * using backtracking.
	 * @param cell is the first cell that its going to check usually x=0, y=0
	 * @return true or false depending if it is 
	 * solvable or not
	 */
	private boolean boolSolve(Square cell) {
		int size = newB.getSize();
		//System.out.println(size);
		
		if(cell == null)
			return true;
		
		int x = cell.getX();
		int y = cell.getY();
		if(newB.getSquare(x, y).getValue() != 0) {
			return boolSolve(getNext(cell));
		}
		
		for(int i = 1 ; i <= size ; i++) {
			boolean valid = isValid(cell, i);
			if(!valid)
				continue;
			newB.setPosition(x, y);
			newB.enterValue(i);
			boolean solved = boolSolve(getNext(cell));
			
			if (solved)
				return true;
			else {
				newB.setPosition(x, y);
				newB.enterValue(0);
			}
		}
		return false;		
	}
	/**
	 * Calls the boolean solve and sets the variable isSolvable of newB to true or false 
	 * depending on whether is solvable or not 
	 * @return the new solved board.
	 */
	public Board solve(){
		cloneBoard();
		newB.isSolvable = boolSolve(new Square(0,0,0));
		return newB;
	}
	
	

}

