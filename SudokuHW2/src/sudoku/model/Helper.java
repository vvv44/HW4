package sudoku.model;

import java.util.List;

/**
 * 
 * @author Victor Enrique Vargas Cornejo
 *
 */


public class Helper {
	/**
	 * Checks the validity of a number by veryfing it does not repeat in column, row or sub-square
	 * 
	 * @param board, the boards to get the numbers from
	 * @param pos, position trying to insert
	 * @param value, value trying to be inserted
	 * @return if the number is valid or not
	 */
	public boolean valid(List<Square> squares, int[] pos, int value, int size){
		if(value==0)
			return true;//here we are trying to eliminate a number
		 /*
		  * Here we are checking if the same column or row have already that number.
		  * */
		 for (Square s: squares) {
            if (s.getValue()==value && (pos[0]==s.getX() || pos[1]==s.getY())) {
                return false;
            }
	     }
		 /*
		  * We will create an instance of a square to temporarily save value.
		  * We get our initial column and row (or the first square of the sub-grid) to do a search trought the whole sub grid
		  * We use two nested loops, given that the squares in the array list have numbers from 0-size*size-1.
		  * That is, we get the position of the initial square (the one in initRow and initCol), from there we check
		  * sqrt(size) squares per iteration in a "vertical way", so the internal loop checks sqrt(size) vertical squares, then
		  * with the outer loop we jump to the next column but on the same row and check again sqrt(size) squares vertically (going down)
		  * In the end we checked the whole sub-grid in which we are trying to place our number.
		  * */
		Square s = new Square();
		int sqrtOfSize = (int)Math.sqrt(size);
		int initRow= (pos[1]/sqrtOfSize)*sqrtOfSize;
		int initCol= (pos[0]/sqrtOfSize)*sqrtOfSize;
		for (Square sq: squares) {
            if (sq.getX() == initCol && sq.getY() == initRow) {
                s = sq;
            }
        }
		for(int i=0;i<sqrtOfSize;i++){
			for(int j=0;j<sqrtOfSize;j++){
				if(squares.get(size*(s.getX()+i)+(s.getY()+j)).getValue()==value)
					return false;
			}
		}
		return true;
	}
	/**
	 * Coming Soon
	 * 
	 * preFillBoard(){
	 * 
	 * };
	 * */
	
	

	
	
	
	
}
