package sudoku.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/** An abstraction of Sudoku puzzle. 
 * 
 * 
 * @author Victor Enrique Vargas Cornejo
 * 
 * 
 * 
 * */
public class Board {

    /** Size of this board (number of columns/rows). */
    public final int size;
    private boolean solved;
    /** Squares of this board. */
    private final List<Square> squares;
	//we will have another variable corresponding with the filling of the board
	/**Given that we keep the user from doing wrong placements, we can assure that when a board is full
	 * is solved. So we'll have a variable that willl keep track of that*/
	private  int filledSpaces = 0;
	/*I will add a variable that will hold, position of a number, so when we call the enter value
	 * we will use the values in this array to modify our main numbers vairable*/
	private int[] position = new int[2];
	//instance of our helper class
	private Helper alfred = new Helper();
	/**Defines if a board is solvable or not.*/
	public boolean isSolvable;
	
	private Filler plug = new Filler();
	
	
	public Board() {
		size=9;//default size is four
		squares = new ArrayList<>(size * size);
        for (int x = 0; x < size; x++) { // store in column-major
            for (int y = 0; y < size; y++) {
                squares.add(new Square(x, y));
            }
        }
	}
	public Board(int s) {
		size=s;
		squares = new ArrayList<>(size * size);
        for (int x = 0; x < size; x++) { // store in column-major
            for (int y = 0; y < size; y++) {
                squares.add(new Square(x, y));
            }
        }
	}
	/**@returns size when called*/
	public int getSize(){
		return size;
	}
	/**
	 * @returns position when called
	 * */
	public int[] getPosition(){
		return position;
	}
	
	/**sets poisition of a value before inserting it
	 * @param x position based on x-axis
	 * @param y position based on y-axis
	 */
	
	public void setPosition(int x, int y){
		//we will set square in previous position as not clicked
		getSquare(position[0],position[1]).setClicked(false);
		position[0] = x;
		position[1] = y;
		//we will set square in such position as clicked
		getSquare(position[0],position[1]).setClicked(true);
	}
	
	/**returns if board is solved or not
	 * 
	 * @return solved
	 */
	public boolean isSolved(){
		return solved;
	}
	/**checks if a number that is trying to input is valid
	 * 
	 * @param n number received from the calling of the method
	 * @return value that method called returns
	 */
	public boolean numIsValid(int n) {
		//Here we will call our helper method to check number validity
		return alfred.valid(squares, position, n,size);
	}
	/**
	 * Returns a non-modifiable list so the values can be checked
	 * */
	  public List<Square> getValues() {
	        return Collections.unmodifiableList(squares);
	    }
	
	/**
	 * It returns a square in a specific position
	 * @param x position based on x-axis
	 * @param y position based on y-axis
	 * @return the square if found
	 * */
	public Square getSquare(int x, int y) {
		for (Square s: squares) {
            if (s.getX() == x && s.getY() == y) {
                return s;
            }
        }
		return null;
    }
	
	/**
	 * CHecks if value to be entered is a delete value or another type, in both cases it enters the value
	 * but in the case of deletion it decreses the value of a field (filledSpaces).
	 * @param value, number that we want to enter in the board.
	 */
	public void enterValue(int value) {
		/**
		 * First we get a reference for the square in the position we are looking for.
		 * */
		Square s = getSquare(position[0], position[1]); 
		if(value == 0){
			s.setValue(value);
			filledSpaces--;//asubstract one one to the count of filled spaces
		}
		else if(s.getValue()!=0){
			return;
		}
		else{
			s.setValue(value);
			filledSpaces++;//add one to the count of filled spaces
		}
		if(filledSpaces==(size*size))
			solved=true;
		
	}
	
	public void fillBoard(){
		plug.fillBoard(squares, size);
		for(Square s:squares)//this is to count off the pre-filled aquares, such that the board can count when is finished
			if(s.getValue()>0){
				filledSpaces++;
				s.setFixed();
			}
			
			
	}
	
	
}
