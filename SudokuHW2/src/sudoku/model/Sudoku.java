package sudoku.model;

import sudoku.dialog.SudokuDialog;

/**
 * 
 * @author Victor Enrique Vargas Cornejo
 *
 */
public class Sudoku {
	/**
	 * Instantiates a new sudoku dialog to begin auguvuhguv game.
	 */
	public static void play(){
	    new SudokuDialog();
	}
	
	/**
	 * 
	 * Coming Soon
	 * playOnline(){
	 * };
	 * */
	
	 public static void main(String[] args) {
	    play();
	 }
}
