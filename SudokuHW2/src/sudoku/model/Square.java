package sudoku.model;
/**
 * Class made for intrinsec properties of every individual square in the sudoku board
 * 
 * @author Victor Vargas
 * */
public class Square {
	private int positionX;
	private int positionY;
	private boolean fixed = false;
	private int value;
	private boolean clicked;
	
	/**
	 * Default constructor
	 * */
	public Square(){
		
	}
	/**
	 * Fills in positions only.
	 * @param x position in x-axis.
	 * @param y, position in y-axis.
	 * */
	public Square(int x, int y){
		positionX=x;
		positionY=y;
	}
	/**
	 * Fills positions and value.
	 * @param v, value in the square (number) 
	 * */
	public Square(int x, int y, int v){
		positionX=x;
		positionY=y;
		value=v;
	}
	/**
	 * @param v, value trying to be set
	 * */
	public void setValue(int v){
		if(!fixed)
			value = v;
	}
	
	/**
	 * Sets v�clicked to true or false
	 * @param clicked, value of clicked
	 * */
	public void setClicked(boolean clicked){
		this.clicked =clicked;
	}
	
	/**Sets the value of fixed to true, so value cannot be modified*/
	public void setFixed(){
		fixed = true;
	}
	
	public int getValue(){
		return value;
	}
	
	public int getX(){
		return positionX;
	}
	
	public int getY(){
		return positionY;
	}
	
	public boolean isClicked(){
		return clicked;
	}
	public boolean isFixed() {
		return fixed;
	}
	
	
}
