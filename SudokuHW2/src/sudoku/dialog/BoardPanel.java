package sudoku.dialog;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JPanel;

import sudoku.model.Board;
import sudoku.model.Square;

/**
 * A special panel class to display a Sudoku board modeled by the
 * {@link sudoku.model.Board} class. You need to write code for
 * the paint() method.
 *
 * @see sudoku.model.Board
 * @author Yoonsik Cheon
 * 
 * 
 * 
 * Modifications done by: Victor Enrique Vargas Cornejo
 * 
 * 
 * 
 */
@SuppressWarnings("serial")
public class BoardPanel extends JPanel {
    
	public interface ClickListener {
		
		/** Callback to notify clicking of a square. 
		 * 
		 * @param x 0-based column index of the clicked square
		 * @param y 0-based row index of the clicked square
		 */
		void clicked(int x, int y);
	}
	
    /** Background color of the board. */
	private static final Color boardColor = new Color(247, 223, 150);

    /** Board to be displayed. */
    private Board board;

    /** Width and height of a square in pixels. */
    private int squareSize;

    /** Create a new board panel to display the given board. */
    public BoardPanel(Board board, ClickListener listener) {
        this.board = board;
        addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
            	int xy = locateSquaree(e.getX(), e.getY());
            	if (xy >= 0) {
            		listener.clicked(xy / 100, xy % 100);
            	}
            }
        });
    }

    /** Set the board to be displayed. */
    public void setBoard(Board board) {
    	this.board = board;
    }
    
    /**
     * Given a screen coordinate, return the indexes of the corresponding square
     * or -1 if there is no square.
     * The indexes are encoded and returned as x*100 + y, 
     * where x and y are 0-based column/row indexes.
     */
    private int locateSquaree(int x, int y) {
    	if (x < 0 || x > board.size * squareSize
    			|| y < 0 || y > board.size * squareSize) {
    		return -1;
    	}
    	int xx = x / squareSize;
    	int yy = y / squareSize;
    	return xx * 100 + yy;
    }

    /** Draw the associated board. */
    @Override
    public void paint(Graphics g) {
        super.paint(g); 
        // determine the square size
        Dimension dim = getSize();
        squareSize = Math.min(dim.width, dim.height) / board.size;
        // draw background
        final Color oldColor = g.getColor();
        g.setColor(boardColor);
        g.fillRoundRect(0, 0, squareSize * board.size, squareSize * board.size,15,15);
        //Paint square that is clicked
        Square clickedSquare = board.getSquare(board.getPosition()[0], board.getPosition()[1]);
        if(clickedSquare.isClicked() && clickedSquare.isFixed()){
        	g.setColor(Color.gray);
        	g.fillRect(squareSize*clickedSquare.getX(), squareSize*clickedSquare.getY(), squareSize, squareSize);
        }else if(clickedSquare.isClicked()){
			g.setColor(Color.GREEN);
			g.fillRect(squareSize*clickedSquare.getX(), squareSize*clickedSquare.getY(), squareSize, squareSize);
        }
        /*We could try to fill several "rectangles" of width or height 1. We could use two loops for the inner grid.
         * I will make the borders a curved rectangle so it looks nicer.*/
        g.setColor(Color.black);
        /*These next three statements give us a rounded corner rectangle for the edges of the board*/
        g.drawRoundRect(0, 0, squareSize * board.size, squareSize * board.size, 15, 15);
        g.drawRoundRect(1,1, squareSize * board.size-2, squareSize * board.size-2, 15, 15);
        g.drawRoundRect(2, 2, squareSize * board.size-4, squareSize * board.size-4, 15, 15);
          
       
        /*For loops that will draw the vertical and horizontal lines, they have a nested loop
         * such that the inner loop draws the thin lines and the outer draws the thicker lines
         * to make the sub-squares more visible.*/
        for(int i=0;i<board.size;i+=Math.sqrt(board.size)){
        	if(i!=0)
        		g.fillRect(0, squareSize*i, squareSize * board.size, 3);
			for(int j=1;j<Math.sqrt(board.size);j++){
            	g.fillRect(0, squareSize*(i+j), squareSize * board.size,1);
        	}	
        	
        }
        for(int i=0;i<board.size;i+=Math.sqrt(board.size)){
        	if(i!=0)
        		g.fillRect(squareSize*i, 0, 3, squareSize * board.size);
        	for(int j=1;j<Math.sqrt(board.size);j++){
            	g.fillRect(squareSize*(i+j), 0, 1, squareSize * board.size);
        	}
        }
        g.setFont(new Font("TimesRoman", Font.PLAIN, squareSize/2));
        /*we will receive a reference to the numbers in order to search which are filled*/
        List<Square> b= board.getValues();
        //go trought the values looking for a number
    	for(Square s:b){
    		if(s.getValue()!=0){//then we have a number
				String n = String.valueOf(s.getValue());
				g.drawString(n,squareSize*(s.getX()+1)-((squareSize*15)/24), (squareSize*(s.getY()+1)-(squareSize/3)));
			}
    	}
    	
    		
    	
    }
}
