package sudoku.dialog;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.net.URL;

import javax.swing.*;


import sudoku.model.Board;
import sudoku.model.Solver;

/**
 * A dialog template for playing simple Sudoku games.
 * You need to write code for three callback methods:
 * newClicked(int), numberClicked(int) and boardClicked(int,int).
 *
 * @author Yoonsik Cheon
 * 
 * 
 * 
 * Modifications done by: Victor Enrique Vargas Cornejo
 * 
 * 
 * 
 * 
 * 
 */
@SuppressWarnings("serial")
public class SudokuDialog extends JFrame {

    /** Default dimension of the dialog. */
    private final static Dimension DEFAULT_SIZE = new Dimension(310, 430);

    private final static String IMAGE_DIR = "/image/";
    
   
    
    /** Sudoku board. */
    private Board board;
    
    /**Sudoku Solver*/
    private Solver solver;
    
    /** Special panel to display a Sudoku board. */
    private BoardPanel boardPanel;

    /** Message bar to display various messages. */
    private JLabel msgBar = new JLabel("");

    /** Create a new dialog. */
    public SudokuDialog() {
    	this(DEFAULT_SIZE,4);
    }
    /** Create a new dialog. with a certain board size*/
    public SudokuDialog(int size) {
    	this(DEFAULT_SIZE,size);
    }
    
    /** Create a new dialog of the given screen dimension. */
    public SudokuDialog(Dimension dim,int size) {
        super("Sudoku");
        setSize(dim);
        board = new Board(size);
        board.fillBoard();
        boardPanel = new BoardPanel(board, this::boardClicked);
        configureUI();
        //setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setVisible(true);
        //setResizable(false);
    }

    /**
     * Callback to be invoked when a square of the board is clicked.
     * @param x 0-based row index of the clicked square.
     * @param y 0-based column index of the clicked square.
     */
    private void boardClicked(int x, int y) {
        /*Here we will modify the variable position in board by giving it the values
         * that this method receives.*/
    	board.setPosition(x, y);
    	showMessage(String.format("Board clicked: x = %d, y = %d",  x, y));
    	if(board.getSquare(x, y).isFixed())
    		showMessage("Square is fixed and cannot be modified.");
    	repaint();
    }
    
    /**
     * Callback to be invoked when a number button is clicked.
     * @param number Clicked number (1-9), or 0 for "X".
     */
    private void numberClicked(int number) {
    	/*Here we can first pass that number to be checked by the board class (or a helper class) to verify its validity.*/
    	if(board.numIsValid(number)){
    		/*if it is valid we will insert the value trought the method enter value*/
    		board.enterValue(number);
    		//here we have to update the display of the board
    		repaint();
    		showMessage("Number clicked: " + number);
    	}
    	else{
    		/*Here we get the directory of the sound file to play it when a number is not valid.*/
			URL url = getClass().getResource("/sound/scream.wav");   
			AudioClip clip = Applet.newAudioClip(url);
			clip.play();
    		showMessage("Number clicked is not valid, number:" + number);
    	}
    	//If user completes the puzzle show a dialogue congratulating
    	if(board.isSolved()){
    		JOptionPane.showMessageDialog(null,"Congratulations, you did it. I always believed in you!<3");
    		showMessage("Congratulations");
    	}
    		
        
    }
    
    /**
     * Callback to be invoked when a new button is clicked.
     * If the current game is over, start a new game of the given size;
     * otherwise, prompt the user for a confirmation and then proceed
     * accordingly.
     * 
     */
    //FIXME: add way to choose difficulty
    private void newClicked() {
    	//We will make an instance of the class, and add the confirm and cancel buttons here to have more control over their actions
    	NewGameDialog newWindow = new NewGameDialog();
    	//Here the buttons for cancel or confirm will be placed.
		JButton confirm = new JButton("Confirm");
		JButton cancel = new JButton("Cancel");
		/**we add action listeners so when we confirm the creation of a new board, we set the right size 
		 * and we dispose of the windows properly.*/
		confirm.addActionListener(e->{
			new SudokuDialog(newWindow.getBSize());
			newWindow.dispose();
			dispose();
		});
		cancel.addActionListener(e -> {newWindow.dispose();});
		newWindow.add(confirm);
		newWindow.add(cancel);
    }

    /**
     * Callback to be invoked when the check for solvability button is called
     * */
    public void isSolvableClicked(){
    	//we initialize our solver.
    	solver = new Solver(board);
    	//Here we do not need to update the global variable since we are only checking if the board is solvable.
    	if(solver.solve().isSolvable)
    		showMessage("Board is Solvable.");
    	else
    		showMessage("Board is NOT solvable");		
    }
    
    /**
     * Callback to be invoked when the button solved is clicked.
     * */
    public void solveClicked(){
    	isSolvableClicked();
    	if(solver.solve().isSolvable){
    		board = solver.solve();
    		boardPanel.setBoard(board);
    		repaint();
    	}
    }
    
    /**
     * Display the given string in the message bar.
     * @param msg Message to be displayed.
     */
    private void showMessage(String msg) {
        msgBar.setText(msg);
    }

    /** Configure the UI. */
    private void configureUI() {
        setIconImage(createImageIcon("sudoku.png").getImage());
        setLayout(new BorderLayout());
        
        JToolBar toolBar = makeToolBar();
        add(toolBar, BorderLayout.NORTH);
        JPanel buttons = makeControlPanel();
        // boarder: top, left, bottom, right
        buttons.setBorder(BorderFactory.createEmptyBorder(10,16,0,16));
        //menus.add(buttons, BorderLayout.SOUTH);
        JPanel board = new JPanel();
        board.setBorder(BorderFactory.createEmptyBorder(10,16,0,10));
        board.setLayout(new BorderLayout());
        board.add(boardPanel, BorderLayout.CENTER);
        //Add number buttons to the same panel as the board
        board.add(buttons, BorderLayout.NORTH);
        add(board, BorderLayout.CENTER);
        
        msgBar.setBorder(BorderFactory.createEmptyBorder(10,16,10,10));
        add(msgBar, BorderLayout.SOUTH);

        JMenuBar menuBar = makeMenu();
        menuBar.setBorder(BorderFactory.createEmptyBorder(10,16,0,16));
        setJMenuBar(menuBar);
    }
    //FIXME: Finish Menu bar shit  
    private JMenuBar makeMenu() {
    	JMenuBar menuBar = new JMenuBar();
    	JMenu menu = new JMenu("Juegillo");
    	menu.setMnemonic(KeyEvent.VK_G);
    	menu.getAccessibleContext().setAccessibleDescription("Game menu");
    	menuBar.add(menu);
    	JMenuItem menuItem = new JMenuItem("New game", KeyEvent.VK_N);
    	menuItem.setIcon(createImageIcon("playIcon.png"));
    	menuItem.setAccelerator(KeyStroke.getKeyStroke(
    	   KeyEvent.VK_N, ActionEvent.ALT_MASK));
    	menuItem.getAccessibleContext().setAccessibleDescription(
    	   "Play a new game");
    	menuItem.addActionListener(e -> {
            newClicked();
        });
    	JMenuItem menuItem2 = new JMenuItem("Solve", KeyEvent.VK_N);
    	menuItem2.setIcon(createImageIcon("solveIcon.png"));
    	menuItem2.setAccelerator(KeyStroke.getKeyStroke(
    	   KeyEvent.VK_N, ActionEvent.ALT_MASK));
    	menuItem2.getAccessibleContext().setAccessibleDescription(
    	   "Apply master solve");
    	menuItem2.addActionListener(e->{
    		solveClicked();
    	});
    	JMenuItem menuItem3 = new JMenuItem("Exit", KeyEvent.VK_N);
    	menuItem3.setAccelerator(KeyStroke.getKeyStroke(
    	   KeyEvent.VK_N, ActionEvent.ALT_MASK));
    	menuItem3.getAccessibleContext().setAccessibleDescription(
    	   "Had too much fun already?");
    	menuItem3.addActionListener(e->{
    		dispose();
    	});
    	menu.add(menuItem);
    	menu.add(menuItem2);
    	menu.add(menuItem3);
    	return menuBar;
    }
    /**Create a tool bar with functions including: start a new game, check if board is solvable and give hints, among others.*/
    //FIXME: Modifiabke for more buttons
    public JToolBar makeToolBar(){
    	JToolBar tulBar = new JToolBar("Sudoku");
    	//Play new game button
    	JButton buttonPlay = new JButton(createImageIcon("playIcon.png"));
    	buttonPlay.addActionListener(e -> {
            newClicked();
        });
    	buttonPlay.setToolTipText("Play a new game");
    	buttonPlay.setFocusPainted(false);
    	tulBar.add(buttonPlay);
    	//Hint button
    	JButton buttHint = new JButton(createImageIcon("hintIcon.png"));
    	//FIXME: add action buttHint.addActionListener();
    	buttHint.setToolTipText("Get a hint of sorts");
    	buttHint.setFocusPainted(false);
    	tulBar.add(buttHint);
    	//check solvable button
    	JButton buttCheck = new JButton(createImageIcon("checkIcon.png"));
    	buttCheck.addActionListener(e->{
    		isSolvableClicked();
    	});
    	buttCheck.setToolTipText("Check if board is solvable");
    	buttCheck.setFocusPainted(false);
    	tulBar.add(buttCheck);
    	//check solvable button
    	JButton buttHelp = new JButton(createImageIcon("helpIcon.png"));
    	buttHelp.addActionListener(e->{
    		JOptionPane.showMessageDialog(this, "Best Sudoku Ever.\n"
    				+ "Created by El Felipin, El Danielin, and El Victorin.\n"
    				+"2018");
    		
    	});
    	buttHelp.setToolTipText("Things about the game");
    	buttHelp.setFocusPainted(false);
    	tulBar.add(buttHelp);
    	
    	return tulBar;
    }
    
    /** Create a control panel consisting of new and number buttons. */
    private JPanel makeControlPanel() {
        
    	// buttons labeled 1, 2, ..., 9, and X.
    	JPanel numberButtons = new JPanel(new FlowLayout());
    	int maxNumber = board.getSize() + 1;
    	for (int i = 1; i <= maxNumber; i++) {
            int number = i % maxNumber;
            JButton button = new JButton(number == 0 ? "X" : String.valueOf(number));
            button.setFocusPainted(false);
            button.setMargin(new Insets(0,2,0,2));
            button.addActionListener(e -> numberClicked(number));
    		numberButtons.add(button);
    	}
    	numberButtons.setAlignmentX(LEFT_ALIGNMENT);

    	JPanel content = new JPanel();
    	content.setLayout(new BoxLayout(content, BoxLayout.PAGE_AXIS));
        content.add(numberButtons);
        return content;
    }

    /** Create an image icon from the given image file. */
    private ImageIcon createImageIcon(String filename) {
        URL imageUrl = getClass().getResource(IMAGE_DIR + filename);
        if (imageUrl != null) {
            return new ImageIcon(imageUrl);
        }
        return null;
    }
    
    /**Create number buttons based on the position clicked and the possible numbers that can be in it.*/
    private JPanel makeNumberButtons() {
    	// buttons labeled 1, 2, ..., 9, and X.
    	JPanel numberButtons = new JPanel(new FlowLayout());
    	int maxNumber = board.getSize() + 1;
    	for (int i = 1; i <= maxNumber; i++) {
            int number = i % maxNumber;
            JButton button = new JButton(number == 0 ? "X" : String.valueOf(number));
            if(board.numIsValid(i))
            	button.setEnabled(false);
            button.setFocusPainted(false);
            button.setMargin(new Insets(0,2,0,2));
            button.addActionListener(e -> numberClicked(number));
    		numberButtons.add(button);
    	}
    	numberButtons.setAlignmentX(LEFT_ALIGNMENT);

    	JPanel content = new JPanel();
    	content.setLayout(new BoxLayout(content, BoxLayout.PAGE_AXIS));
        content.add(numberButtons);
        return content;
    }

   
}
