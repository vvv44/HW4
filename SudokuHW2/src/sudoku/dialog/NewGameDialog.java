package sudoku.dialog;

import java.awt.*;
import javax.swing.*;

/**
 * Dialog to show when user clicks new game option
 * */
@SuppressWarnings("serial")
public class NewGameDialog extends JFrame{
	/** Default dimension of the dialog. */
    private final static Dimension DEFAULT_SIZE = new Dimension(240,240);
	/**Size of the board ot create. Four by default*/
    private int size = 4;
    /**Variable to see if new game is confirmed*/
    private boolean confirmed = false;
    
    final static boolean shouldFill = true;
    final static boolean shouldWeightX = true;
    private JPanel chooseSizePan = new JPanel();
    private JPanel chooseDifPan = new JPanel();
	
	public NewGameDialog(){
		this(DEFAULT_SIZE);
	}
	
	public NewGameDialog(Dimension dim){
		super("New Game");
		setSize(dim);
		configureOptions();
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setVisible(true);	
        setLayout(new GridLayout(4,3));
	}
	public boolean getConfirmed(){
		return confirmed;
	}
	public int getBSize(){
		return size;
	}
	
	/**
	 * Configures visual options of size and difficulty of sudoku
	 * */
	private void configureOptions() {
		chooseSizePan.setLayout(new GridLayout(0,2));
		chooseDifPan.setLayout(new GridLayout(0,3));
		JRadioButton[] sizeButtons = new JRadioButton[2];
		/*SIZE CHOOSING PANEL BEGIN ADDING ELEMENTS*/
		JLabel label = new JLabel("Choose Size.");      
		add(label);
		sizeButtons[0] = new JRadioButton("4x4");
		sizeButtons[0].addActionListener(e->{size=4;});
		chooseSizePan.add(sizeButtons[0]);
		sizeButtons[1] = new JRadioButton("9x9");
		sizeButtons[1].addActionListener(e->{size=9;});
		chooseSizePan.add(sizeButtons[1]);
		
		ButtonGroup sizeGroup = new ButtonGroup();
		sizeGroup.add(sizeButtons[0]);
		sizeGroup.add(sizeButtons[1]);
		sizeButtons[0].setSelected(true);
		sizeGroup.setSelected(sizeButtons[0].getModel(), true);
		add(chooseSizePan);
		/*SIZE CHOOSING PANEL FINISH ADDING ELEMENTS*/
	}
	

}
