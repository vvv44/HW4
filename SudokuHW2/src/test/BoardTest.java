package test;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import sudoku.model.Board;

@RunWith(value = Parameterized.class)
public class BoardTest {
	private Board b;
	private int x,y,num;
	
	@Parameters
	public static Iterable<Object[]> dataForConstruct(){
		return Arrays.asList(new Object[][] {{0,8,1},{8,0,1},{4,5,1},{5,4,1},{7,3,1},{4,0,1}});
	}
	public BoardTest(int x, int y,int num){
		this.x = x;
		this.y = y;
	}
	
	
	@Before
	public void prepare(){
		b = new Board();
	}
	
	@Test
	public void TestDefConstr(){
		assertEquals(9,b.getSize());
	}
	@Test
	public void TestConstr(){
		b = new Board(4);
		assertEquals(4,b.getSize());
	}                        
	
	
	@Test
	public void testSetPosition() {
		b.setPosition(x,y);
		assertEquals(x,b.getPosition()[0]);
		assertEquals(y,b.getPosition()[1]);
	}

	@Test
	public void testIsSolved() {
		//the solved is by default a false 
		assertEquals(false,b.isSolved());
	}

	@Test
	public void testNumIsValid() {
		b.setPosition(x, y);
		assertEquals(true,b.numIsValid(num));
	}

	@Test
	public void testEnterValue() {
		//Here we will need to try entering a value, and then get the square with that value, and check if it got entered correctly.
		b.setPosition(x, y);
		b.enterValue(num);
		assertEquals(num,b.getSquare(x, y).getValue());
	}

}
