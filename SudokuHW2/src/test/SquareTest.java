package test;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import sudoku.model.Square;

@RunWith(value = Parameterized.class)
public class SquareTest {
	private Square s;
	private int x,y,v;
	
	@Parameters
	public static Iterable<Object[]> dataForConstruct(){
		return Arrays.asList(new Object[][] {{0,9,1},{9,0,9},{4,5,5},{5,4,4}});
	}
	
	
	public SquareTest(int x,int y, int v){
		this.x = x;
		this.y= y;
		this.v = v;
	}
	
	@Before 
	public void prepare(){
		s = new Square();
	}
	
	@Test
	public void testDefConstruct(){
		assertEquals(0,s.getX());
		assertEquals(0,s.getY());
		assertEquals(0,s.getValue());
	}
	
	@Test
	public void testConstr(){
		s = new Square(x,y,v);
		assertEquals(x,s.getX());
		assertEquals(y,s.getY());
		assertEquals(v,s.getValue());
	}
	
	
	
}
